package crawler

import org.slf4j.LoggerFactory
import java.net.URL

class Crawler(private val maxDepth: Int,
              private val siteMap: SiteMap,
              private val parser: Parser,
              private val bodyProvider: BodyProvider = HttpBodyProvider()) {

    private val log = LoggerFactory.getLogger(javaClass)
    private val visited: MutableSet<UrlWrapper> = mutableSetOf()

    fun populateSiteMap(url: URL, depth: Int = 0, parent: URL? = null) {
        if (depth == maxDepth) {
            /*
             * This is needed even if loop detection would work properly, because
             * pages may generate multiple links leading to the same resource.
             */
            log.debug("Too deep {}", visited)
            return
        }
        val urlWrapper = UrlWrapper(url) // Wrapper here for better URLs comparison
        if (visited.contains(urlWrapper)) {
            /*
             * This will fail if a web page contains links to the same resource
             * with dynamically generated parameters, like timestamps or tokens.
             */
            log.debug("Loop detected: from {} to {}", parent, url)
        } else {
            if (parent != null) {
                siteMap.add(parent, url)
            }
            val body = bodyProvider.get(url)
            val tempUrls = parser.parseBody(url, body)
            visited.add(urlWrapper)
            tempUrls.forEach { populateSiteMap(it, depth + 1, url) }
        }
    }
}
