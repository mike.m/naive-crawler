package crawler

import org.slf4j.LoggerFactory
import java.net.URL

private val log = LoggerFactory.getLogger("main")

fun main(args: Array<String>) {

    val confStartingUrl = args.getOrElse(0) {
        "http://127.0.0.1:8080"
    }
    val confMaxDepth = args.getOrElse(1) { "20" }
    val maxDepth = confMaxDepth.toIntOrNull() ?: 20

    try {
        val url = URL(confStartingUrl)
        val siteMap = SiteMap(url)
        log.info("Crawling {} with max depth {}", url.toExternalForm(), maxDepth)
        Crawler(maxDepth, siteMap, Parser(url.host))
                .populateSiteMap(url)
        printSiteMap(siteMap.rootUrl)
    } catch (e: Exception) {
        log.error("Can't start: {}", e.message, e)
    }
}

fun printSiteMap(rootUrl: UrlWrapper) {
    println("Site map")
    traverse(rootUrl)
}

private fun traverse(url: UrlWrapper, visited: List<UrlWrapper> = listOf()) {
    if (visited.contains(url)) {
        printPath(visited)
    } else {
        val newVisited = visited.plus(url)
        if (url.children.isEmpty()) {
            printPath(newVisited)
        } else {
            url.children.forEach { traverse(it, newVisited) }
        }
    }
}

private fun printPath(urls: List<UrlWrapper>) {
    println(urls.map { it.url }.joinToString(" -> ") { it.toExternalForm() })
}
