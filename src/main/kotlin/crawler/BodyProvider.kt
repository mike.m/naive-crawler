package crawler

import org.slf4j.LoggerFactory
import java.net.URL

interface BodyProvider {
    fun get(url: URL): String
}

private const val textHtml = "text/html"

class HttpBodyProvider : BodyProvider {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun get(url: URL): String {
        return try {
            val response = khttp.get(url.toExternalForm(), headers = mapOf("Accept" to textHtml))
            if (response.statusCode == 200 && response.headers.getOrDefault("Content-Type", "").startsWith(textHtml)) {
                response.text
            } else {
                ""
            }
        } catch (e: Exception) {
            log.debug("Could not read body: {}", e.message, e)
            ""
        }
    }
}
