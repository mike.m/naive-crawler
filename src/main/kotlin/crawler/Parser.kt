package crawler

import org.slf4j.LoggerFactory
import java.net.URL
import java.util.regex.Pattern

class Parser(private val baseHost: String) {

    private val log = LoggerFactory.getLogger(javaClass)

    /*
     * This may catch wrong URL starting with single quote but ending with double quote
     * but I decide to leave it as it is. Unlikely to happen and since this is really
     * naive implementation it should be OK. On production I'd use some external lib
     * for parsing HTML response.
     */
    private val urlFromATag = Pattern.compile("(?i)<a[^>]+?href[\\s]*=[\\s]*['\"](.+?)['\"]")

    /*
     * This will catch only HREF links within A tag. No LINK tag support, nothing from
     * HEAD section will work either, like CSS or scripts. But again, this is not
     * a production grade solution.
     */
    fun parseBody(url: URL, body: String): Set<URL> {
        val matcher = urlFromATag.matcher(body)
        val children = mutableSetOf<URL>()
        val base = URL(url, ".").toExternalForm().trimEnd('.')
        while (matcher.find()) {
            /*
             * I am trimming starting slash here as it seems to have a meaning only when
             * BASE element is present. As stated above, nothing from HEAD section is
             * supported.
             *
             * Then anchor is removed, as all anchors points to the same page.
             */
            val found = trimAnchor(matcher.group(1).trimStart('/'))

            if (found.isBlank()) continue
            val child = if (isHttpOrHttps(found)) URL(found) else URL("$base$found")
            if (child.host == baseHost) {
                children.add(child)
            } else {
                log.debug("External URL ignored: {}", child)
            }
        }
        return children
    }
}

fun trimAnchor(text: String): String {
    val anchorIndex = text.indexOf('#')
    return if (anchorIndex == -1) {
        text
    } else {
        text.substring(0, anchorIndex)
    }
}

private fun isHttpOrHttps(url: String) = url.startsWith("http://") || url.startsWith("https://")
