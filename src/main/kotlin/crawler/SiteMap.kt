package crawler

import java.net.URL
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap

class SiteMap(root: URL) {

    private val urls: ConcurrentMap<URL, UrlWrapper> = ConcurrentHashMap()
    val rootUrl: UrlWrapper = UrlWrapper(root)

    init {
        urls[root] = rootUrl
    }

    fun add(from: URL, to: URL): Boolean {
        val fromWrapper = urls[from]
        return if (fromWrapper == null) {
            false
        } else {
            val toWrapper = urls.getOrPut(to) { UrlWrapper(to) }
            return fromWrapper.children.add(toWrapper)
        }
    }
}

data class UrlWrapper(
        val url: URL,
        val children: MutableSet<UrlWrapper> = mutableSetOf()) {

    override fun hashCode(): Int {
        return url.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return when (other) {
            null -> false
            is UrlWrapper -> Objects.equals(url.protocol, other.url.protocol) &&
                    Objects.equals(url.host, other.url.host) &&
                    Objects.equals(url.port, other.url.port) &&
                    Objects.equals(url.file, other.url.file)
            else -> false
        }
    }
}
