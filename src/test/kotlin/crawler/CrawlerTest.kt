package crawler

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.net.URL

internal class CrawlerTest {

    private val pageIndex = URL("http://aaa")
    private val pageSupport = URL(pageIndex, "support.html")
    private val pageAbout = URL(pageIndex, "about.html")
    private val pageProducts = URL(pageIndex, "products.html")
    private val siteMap: SiteMap = SiteMap(pageIndex)
    private val parser = Parser(pageIndex.host)

    private fun getCrawler(b: (URL) -> String): Crawler {
        return Crawler(3, siteMap, parser, object : BodyProvider {
            override fun get(url: URL) = b(url)
        })
    }

    @Test
    fun excludeSelf() {
        val crawler = getCrawler {
            when (it) {
                pageIndex -> """
                        <a href='${pageIndex.toExternalForm()}'>home</a> <- exclude this link
                        <a href='${pageProducts.toExternalForm()}'>products</a>
                        <a href='${pageSupport.toExternalForm()}'>support</a>
                        <a href='${pageAbout.toExternalForm()}'>about</a>
                    """.trimIndent()
                else -> ""
            }
        }
        crawler.populateSiteMap(pageIndex)
        Assertions.assertEquals(3, siteMap.rootUrl.children.size)
        Assertions.assertTrue(siteMap.rootUrl.children.any { it.url == pageProducts })
        Assertions.assertTrue(siteMap.rootUrl.children.any { it.url == pageSupport })
        Assertions.assertTrue(siteMap.rootUrl.children.any { it.url == pageAbout })
    }

    @Test
    fun loopDetection() {
        val crawler = getCrawler {
            when (it) {
                pageIndex -> "<a href='${pageAbout.toExternalForm()}'>link</a>"
                pageAbout -> "<a href='${pageIndex.toExternalForm()}'>link</a>"
                else -> ""
            }
        }
        crawler.populateSiteMap(pageIndex)
        Assertions.assertEquals(1, siteMap.rootUrl.children.size)
        Assertions.assertTrue(siteMap.rootUrl.children.any { it.url == pageAbout })
    }

    @Test
    fun externalLinkExcluded() {
        val crawler = getCrawler {
            when (it) {
                pageIndex -> "<a href='http://external.page/'>link</a>"
                else -> ""
            }
        }
        crawler.populateSiteMap(pageIndex)
        Assertions.assertEquals(0, siteMap.rootUrl.children.size)
    }

    @Test
    fun tooDeep() {
        val crawler = getCrawler {
            when (it) {
                pageIndex -> "<a href='${pageAbout.toExternalForm()}'>link</a>"
                pageAbout -> "<a href='${pageSupport.toExternalForm()}'>link</a>"
                pageSupport -> "<a href='${pageProducts.toExternalForm()}'>link</a>"
                pageProducts -> "<a href='${pageIndex.toExternalForm()}/too-deep-here'>link</a>"
                else -> ""
            }
        }
        crawler.populateSiteMap(pageIndex)
        Assertions.assertEquals(1, siteMap.rootUrl.children.size)
        val about = siteMap.rootUrl.children.find { it.url == pageAbout }
        Assertions.assertNotNull(about)
        Assertions.assertEquals(1, about!!.children.size)
        val support = about.children.find { it.url == pageSupport }
        Assertions.assertNotNull(support)
        Assertions.assertEquals(0, support!!.children.size)
    }

    @Test
    fun multiplePagesLinkOnePage() {
        val crawler = getCrawler {
            when (it) {
                pageIndex -> "<a href='${pageAbout.toExternalForm()}'>link</a><a href='${pageSupport.toExternalForm()}'>link</a>"
                pageAbout -> "<a href='${pageProducts.toExternalForm()}'>link</a>"
                pageSupport -> "<a href='${pageProducts.toExternalForm()}'>link</a> this is duplicate, should not be listed"
                else -> ""
            }
        }
        crawler.populateSiteMap(pageIndex)
        Assertions.assertEquals(2, siteMap.rootUrl.children.size)
        val about = siteMap.rootUrl.children.find { it.url == pageAbout }
        Assertions.assertNotNull(about)
        Assertions.assertEquals(1, about!!.children.size)
        val support = siteMap.rootUrl.children.find { it.url == pageSupport }
        Assertions.assertNotNull(support)
        Assertions.assertEquals(0, support!!.children.size)
    }

}
