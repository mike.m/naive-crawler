package crawler

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.net.URL

internal class ParserTest {

    private val base = URL("http://127.0.0.1/")
    private val parser = Parser(base.host)

    @Test
    fun whitespace() {
        val body = """
            <a href =
            'aaa'><a                    href='bbb'>
        """.trimIndent()
        val urls = parser.parseBody(base, body)
        Assertions.assertEquals(2, urls.size)
        Assertions.assertTrue(urls.contains(URL("http://127.0.0.1/aaa")))
        Assertions.assertTrue(urls.contains(URL("http://127.0.0.1/bbb")))
    }

    @Test
    fun hrefSecond() {
        val body = """
            <a aaa href='bbb'>
        """.trimIndent()
        val urls = parser.parseBody(base, body)
        Assertions.assertEquals(1, urls.size)
        Assertions.assertTrue(urls.contains(URL("http://127.0.0.1/bbb")))
    }

    @Test
    fun anchorRemoved() {
        val body = """
            <a href='bbb#anchor'>
        """.trimIndent()
        val urls = parser.parseBody(base, body)
        Assertions.assertEquals(1, urls.size)
        Assertions.assertTrue(urls.contains(URL("http://127.0.0.1/bbb")))
    }

    @Test
    fun keepParams() {
        val body = """
            <a href='bbb?key=value'>
        """.trimIndent()
        val urls = parser.parseBody(base, body)
        Assertions.assertEquals(1, urls.size)
        Assertions.assertTrue(urls.contains(URL("http://127.0.0.1/bbb?key=value")))
    }

    @Test
    fun fullyQualifiedUrl() {
        val body = """
            <a href='http://127.0.0.1/aaa'>
        """.trimIndent()
        val urls = parser.parseBody(base, body)
        Assertions.assertEquals(1, urls.size)
        Assertions.assertTrue(urls.contains(URL("http://127.0.0.1/aaa")))
    }

    @Test
    fun externalExcluded() {
        val body = """
            <a href='http://127.0.0.2/bbb'>
        """.trimIndent()
        val urls = parser.parseBody(base, body)
        Assertions.assertTrue(urls.isEmpty())
    }

    @Test
    fun startingSlashTrimmed() {
        val body = """
            <a href='/abc'>
        """.trimIndent()
        val urls = parser.parseBody(base, body)
        Assertions.assertEquals(1, urls.size)
        Assertions.assertTrue(urls.contains(URL("http://127.0.0.1/abc")))
    }

    @Test
    fun coversSingleAndDoubleQuotes() {
        val body = """
            <a href='/aaa'>
            <a href="/bbb">
        """.trimIndent()
        val urls = parser.parseBody(base, body)
        Assertions.assertEquals(2, urls.size)
        Assertions.assertTrue(urls.contains(URL("http://127.0.0.1/aaa")))
        Assertions.assertTrue(urls.contains(URL("http://127.0.0.1/bbb")))
    }
}
